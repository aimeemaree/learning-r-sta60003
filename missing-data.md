Missing Data

In R missing data is represnted as NA or NaN
NA - Not Available
NaN - Not a Number

If you import data missing data could be represented as a different symbol based on the files input for SPSS it could be 99 but you can not assume this. If you are unaware of the patters for missing data you can run checks on some more likely used symbols to see if you can pick up a pattern and also look at a sample of data and each variables. Running a summary() on a dataset will let you know if there are missing values in a variable. This can narrow down the search for missing value patterns down to specific columns, the way to see this could be more through outliners then what R reports as R will be looking for NA or NaN as its missing value so if 99 is used then a way to see there are missing values would be checking for outliners if the expected reutrn of data values is 1 to 10 and out of 500 observations 34 are 99 this is a good indication these are missing values.


Reasons can vary:
  > is it random, but dangerous to assume
  > is it associated with the variable/outcome of interest
  > is it an empty/null value -> empty string "" or null
  > is there a random special character present that is unexpected "." "_"
  > is there an odd numner of outliners
  > are the ouliners all in a numner far out of the expected range

Special Values

  > NA  > Not Availabe this represents a missing value  any(is.na(x)) #wrapped in any prints one TRUE or FALE if any NA is found, is.na(x) byt itself prints out a TRUE/FALSE for every value  > sum(is.na(x)) calculates the total of TRUE (TRUE values = 1, FALSE values = 0) because in R T/F is binary sum will count all the T as 1 and give us a total that also represents the units found.
  > inf > infinite value, if this an outliner? has a computation gone wrong? accidently dividing by 0? ex: 1/0, 1/0 + 1/0, 33333 ^ 33333   is.NaN()
  > NaN > might not be missing values could be that the class set correct on the variable? accidently dividing by 0? ex: 0/0, 1/0 - 1/0



If you need to remove your data:

  > Make sure you have done a good anaylisis of all variables found with missing values
  > The impact removing them could have on overall observations, for control/set group you may have to remove an observation from the other group if you need to keep numbers balanced of % of specific variables (drug trials could warrant needing 50% observations exposed to X or taking Y in one or many groups)
  > how would/could they skew the distribution, if you have to remove a high number of observations it could impact of the mean/median of certain variables. If you had 30 people and you needed 1/3 who played Tennis however 6 people made a mistake on their brand of toothpaste entry and you decided to remove them but these 6 people also play tennis so removing them will mean you will not have 1/3 of observations playing tennis. Do you keep the 6 people and if so what impact does that have on other variables and how can that be factored in? Do you remove them and then remove random other observations in other groups so they reflect the needed 1/3 balance again?

complete.cases(x) > returns true for a row that is complete and false is one variable has a missing values
na.omit(x) > removes any rows with missing values





