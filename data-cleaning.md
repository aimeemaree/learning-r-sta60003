# Data Cleaning

Automate all the assumptions we can safely make across our datasets

> dates fall into expected periods
> counts of items add up correctly
> persons age can not be negative, year can not be negative, month can not be negative etc
> consitent value levels for categorical data
> check variable names fit into expected patterns
> if Boolean expcted test for TRUE / FALES

Removing missing data from your analysis, this is something that has to be taken into consideration for
each dataset. Keeping missing values can affect the overall quality of the analysis, however in Research
we have a duty to note and capture why we are removing them and what the impact will be on the outcome.

You need to take into account how this will affect your populaiton sample numbers.

Checking for duplicates values, are you expecting them? does it indicate problems with the dataset or specific entries.

you can check duplicated observations
duplicated()

Missing values in R are represented by NA or NaN
