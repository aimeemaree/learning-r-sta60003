# Vectors have the properties of structure and type

# We can check the structure using the str() function
# R can tell us the type of vector, the dimensions and a sample of the values

students <- c("Aimee","Tom","Harry","Ann")
students
str(students)

# We can check if an object is a vector
x <- 2
is.vector(x)
is.vector(students)

# We can check the lenght of a vector
length(students)
length(x)
