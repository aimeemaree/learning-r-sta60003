data(islands, package="datasets")
islands

set.seed(123)

# Lets look at a random sample
rsample <- islands[sample(length(islands), 10, replace = T)]

# lets check for duplicated observations
duplicated(rsample)

# We can use which to find the positons of the duplicate observations
which(duplicated(rsample))

# To remove the duplicates
rsample[!duplicated(rsample)]
rsample[-which(duplicated(rsample))]

x <- c(1, NaN, NA, 10)
# returns true if X is missing
is.na(x)
# returns true if X is NaN ( not a number )
is.nan(x)

# complete.cases function detects rows that do not contain any missing values
complete.cases(x)

# to leave out records with missing values use na.rm
mean(x, na.rm=TRUE)
mean(x[complete.cases(x)])
mean(x)

# Replace missing values coded in 99 with 99
x <- c(10,4,45,56,99,99,7,99,99)
x[x == 99] <- NA
x

# conver Boolean to Numeric
c <- c(TRUE,FALSE,TRUE,FALSE,TRUE)
c[c == TRUE]<- 1
c

d <- c(TRUE,TRUE,TRUE,FALSE,TRUE,FALSE)
d <- as.numeric(d)
d

# Remove NA entries
which(is.na(x))
x
h <- which(is.na(x))
h
