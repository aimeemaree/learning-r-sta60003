The vast Majority of Real World Data is DIRTY!
Paddling in a sea of data what do we do before we drown in plastic?
Attempt to clean it :)

There are four key steps in Data Science:
  Collecting data
  Cleaning the data
  Analysiing the data
  Reporting on the data


Cleaning the data is the most time consuming step of Data Science, it can often be one of the most overlooked steps as well because it is not as "sexy" as the other areas.


Cleaning Data has three crucial steps:
  Exploring raw data
  Tidying the data
  Preparing the data


Exploring Raw Data: can be broken down into three steps
  1: understand the structure
  2: Look the data
  3: Visualise the data

1: Understand the data

We can look into the class of the dataset which whill provide its type, the dimesions of the dataset, and the column names
class()
dim()
names()

Structure allows us to have a deeper look at the observations, variables and datatypes
str()

Using the dplyr package allows us to use glimpse which is a nicer version of str, there are also many other calls you can use with dplyr for data wrangling.
glimpse()

Summary provides a 5 Number summary output 
summary() 

2: Look at the data
Most datasets are large and printing them out directly to the screen to have a look is not optional or even possible with larger sets then in the examples. Due to this we can use the head() and tail() functions to have a look at some of the entries in our dataset to notice any anomolies or what columns need their data-type changed to suit their data etc.
head()
tail()

3: Visualise
Two graphs we can use to take a quick look at our data and will help us indetify any possible skew and outliners.

For one Vector 
A histogram, created with the hist() function, takes a vector (i.e. column) of data, breaks it up into intervals, then plots as a vertical bar the number of instances within each interval. 

To Look at two Vectors
A scatter plot, created with the plot() function, takes two vectors (i.e. columns) of data and plots them as a series of (x, y) coordinates on a two-dimensional plane.


Tidying the data

One of our main aims here is to gather columns into key-value pairs

Wide vs Long 
A wide dataset represents key attributes of the data horizontialy in a table instead of vertically, the opposite is true for long datasets

Wide:
When you have columns that are not variables and you want to collapse them into key-value pairs.
gather(wide_df, my_key, my_val, -col)

Long:
When values in a column should actually be column names (i.e. variables). What we need to do here is indentify key-values pairs and then spread them across multiple columns. This can make the data more compact and easier to read.
spread(long_df, my_key, my_val)

The separate() function allows you to separate one column into multiple columns. Unless you tell it otherwise, it will attempt to separate on any character that is not a letter or number. You can also specify a specific separator using the sep argument.

The opposite of separate() is unite(), which takes multiple columns and pastes them together. By default, the contents of the columns will be separated by underscores in the new column, but this behavior can be altered via the sep argument.


Preparing Data ->

under the hood, the logical values TRUE and FALSE are coded as 1 and 0, respectively. Therefore, as.logical(1) returns TRUE and as.numeric(TRUE) returns 1.


TODO:
Cohercions:

Missing Values: 
see missing-data.md

Outliers:

Example reaons why there could be outliers:
  > Valid measurements
  > Variability in measurement
  > Experimental error
  > Data entry error
  > Could be missing value symbol
  > mesurement error / data entry error
  
  

  




