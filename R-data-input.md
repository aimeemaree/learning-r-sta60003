
R has its own data type RData format
R can input data from various file types often with addtional packages being needed 

an R Data file called "my_data" can be loaded into R using 
load("my_data.RData")
In order to load the file it must be sitting in your working directory

To save a dataframe object as an RData file
save(my_data, file = "my_data.RData")


The function read.table() returns a data.frame and is used to read in the data from a data file. The function reads all the columns as character vectors and tries to turn them into logical, integer, numeric and complex, if it can not convert the vector it will leave it and move onto the next vector to convert. 

You can control which class is assigned to each column and then R will not try to guess what conversion to use. 
colClasses 

If the first record in the file is a header record 
header = TRUE

You also can define the seperator 
sep = ""

by default it is assumed the file will contain the character string NA you can change this with
na.strings

It is important to note the data format, so much of data cleaning and preperation is ensuring the date format is right in my experience. You will need to use one of the two formats for your dataset. 
%Y-%m-%d or Y%m%d

If you want to supress the conversion of character vectors to factors 
as.is = TRUE


I tried to read excel file with packages xlsx however this package required additional packages looking on the internet I noticed how the R community recommends the use of read_excel as it requires no dependent packages 

haven pacakge is for reading SPSS data I had no problems with this one and I could also use the getwd() function to make the code more portable between systems so the path is not hard coded 

You can also save your dataset/dataframe into Rdata format or other formats for example you can pull in a SPSS file and then you can save the work as an RData file 

save() function will save the data in an RData format if you want to save it into another format you need to add some packages and not use save()

save() -> RData File
write.table() -> tab delimited text file
write.csv() -> csv format
write_sav() -> Save in SPSS format




save(my_spss_data, file = "my_data.RData")
