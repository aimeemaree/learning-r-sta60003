# To list all the objects available in the working environment
ls()

# To access the class of an object class()
fives <- c(555,55,5)
class(fives)

# To get the dimensions of an object length() or dim()
length(fives)

# Remove all variables
rm(list = ls())

# To remove one object
rm(fives)
