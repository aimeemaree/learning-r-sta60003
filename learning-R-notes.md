**Notes on R Language**

R was created by Ross Ihaka and Robert Gentleman at the University of Auckland - New Zealand

R will compute only when the commands are syntatically correct. This means that the names of objects in R are Case Sensitive. This means R will throw an [object not found] error when you get the case wrong and it will see x and X as two different objects.

Comments in R are identified with # character

R is object orientated, in R everything we create is an object.

* <-  is the assignemnt operator  x <- 10
* =   is only used to specify values of arguments in functions  read.csv(file="folder/file.csv")
* '#'   identifies comments   # Here is my comment

There are subtle differences between the class, type, and mode of an object

Commands

help.start()            -- opens help manual
setwd(directoryName)    -- sets the working directory name
getwd()                 -- prints the working directory
rm(list= ls())          -- clears all objects in memory : remove

up/down keyboard arrows allow you to scroll through previously entered commands. 
Pressing Ctrl+Up/Command+Up will give you a list of command that you can select from.


An Object in R is a Data Structure
Everything in R is an Object
Data Objects can store numerical or logical or character values
<- and = are not equivilant even though they can be used to produce what seems to be the same outcome

R has the following types of data structure

scalars [Single Type 0-dimensional]   - think of int
vectors [Array of Numbers 1-dimensional]   - think of array
matrices [Table of Data - 2-dimensional]   - think of data map/table/grid
lists [combination of objects of different types] think of super array :: list


Scalar
A Scalar creates a single type object. 

Creating a Scalar object

* x <- 30   Create the x object and assigns the number 30
* x         Print the value of the object

Arithmetic Operators
+ addition
- subtraction
* multipilcation
/ divison
^ or  ** exponentiation

Logical Operators
> greater then
< less then
== exactly equal to
! = not equal to


Integer and Modulus Divisions

Integer division :: x %/% y   x is divided by y but it is rounded down
Modulus division :: x %% y is the remainder of x divided by y (x mod y)


Reserved Words
?reserved -- opens Reserved Word help list

if 
else
repeat
while
function
for
in
next
break
TRUE
FALSE
NULL
Inf
NaN
NA
NA_integer
NA_real
NA_complex
NA_character


When checking the properties of a vector we can use built in functions
typeof()  -- type of vector
is.vector()  -- returns boolean TRUE if object is a vector
lenght()  -- returns length of the vector (how many values the vector holds)
str()  -- returns the structure of the vector : type, dimensions, sample of values

There are four types of Atomic Vectors

Logical - holds TURE or FALSE values : T or F abbriviated - boolean
Character vectors can contain letters and words - strings
Numeric Vectors holds whole numbers and decimal places - doubles
Integer Vectors are a subclass of Numeric and holds whole numbers when the numbers are displayed R places an L to the right of the numbers - integer

There are two mor types of vectors complex for when we use complex numbers such as minus sqaure root of one and also raw vectors which are mainly used by the system

Vector Manipulation

Accessing Vectors
Vectors can be accessed using the vector index, this can be logical, integer or character
Vector index in R starts from 1 unlike most programming languages which start at 0
indexes must be integers and if we happen to use real numbers by accident they will be converted to integer and decmial places ignored

Modifying Vectors

We can modify specific vectors we access and we can also truncate them and use reassignments

Coercion Rule ordering from Least General to Most General
Logical -> Integer -> Numeric -> Character

Logical Operators for Vectors
(TRUE == TRUE) == TRUE returns TRUE
(FALSE == TRUE) == FALSE returns TRUE

Equality operator can be used to compare numbers
Logical operatiors can be chained together
Any () will return true if one or more of the elements in the logical vector is TRUE
All () will return trus is every element in the logcial vector is TRUE


#ToDO Update below with notes

Matrices

matrix()  - converts vector into a matrix
rbind()   - binds matrices or vectors by row on top of the other to create a new matrix
cbind()   - binds matrices or vectors by coloumn next to eachother to create a new matrix

Can define number of columns nsol and rows nrow
You do not need to define columnN and rowN, if only one is defined the otheris inferred from the length of the data.
By default a matrix is filled column-wie unless byrow = TRUE then it is filled in row order.

matrix(11:20, nrow=2, byrow = TRUE)


DataFrames
Dataframes can contain variables of different types. A dataframe requires that each component (vector,matrices) is the same length. R will create a dataframe that accomodates columns with different data types. To force a component to be a character vector we sue stringsAsFactors=FALSE
To add and update dataframes we use the rbind() and cbind() functions as we do with matrices

data.frame() -> creates a dataframe


Factors
Factors are matrices of characters and have specific functions that are suited to characters [non numerical]

Lists

Data Structures
One Dimensional Data = vector 
Nx Dimensional Data = matrix, list, dataframe
  matrices suitable for single data type (numerical or character)
  list and data frames suitable for mixture of data types 


