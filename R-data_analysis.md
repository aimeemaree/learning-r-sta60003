**Notes R Summary Statistics**

Choosing which summary statistics to use depends on our data:

When describinbg or examing data, you will be typically concerned with measures of 

* Location: central tendency - measure of the values of the data > mean and median
* Variation: dispersion - how far data points lie from one another > [normal distribution] Standard Deviation, Coefficent variation [skewed distribution] Percentiles, interquartile range 
* Shape: distribtuion of values - histograms, boxplot, stem plot - statistics include skewness kurtosis - type of distribution normally-distributed, uniform, skewed, bi-modal, etc

We have built in R functions to obtain summary statistics 

summary()   -> five number summary
stat.desc)  -> descriptive statistics (pastecs)
describeBy() -> summary statistics by a grouping variable (psych)

frequencey table (contigency table is used to describe categorical variables)
We can create a one-way 
table(x)
or two-way frequency table
table(x,y)

**Plotting Functions**
High-Level plotting functions create a new plot (axes, labels, titles)
Low-Level plotting functions add more information to an exsisting plot (extra points, lines, labels)
Interactive graphics functions allows you to add information to an exsisting plot or to extract information form an exsisting plot

Pie-Chart are often not recommended as they are hard to interpret the differences and they are rarely sufficent to present detailed statistical information
Bar-Chart plotting categorical data [ordinal and nominal]
Historgram plotting discrete or continous data, displays the shape of the data and can indicate if there are any outliners
Boxplot plotting numerical data as a histogram it also displays shape of the data it will also visually highlight any outliners. A boxplot draes the following measures, Median, Quartiles, Outliners. Good to use to compare multiple subsets. Unlike a hisrogram a boxplot can not indetify modes (unimodal or bimodal).

plot.new()
par(mar=c(bottom, left, top, right))

How coordinates are mapped
Axis Scales plot.window(xlim = xlimits, ylim = ylimits)
plot.new(xlim = c(0, 100), ylim = c(-2, 15))

Drawing Axes - 
side=1 below the graph (xaxis) 
side=2 to the left of the graph (yaxis) 
side=3 above the graph(xaxis) 
side=4 to the right of the graph (yaxis)

axis(side) 

Axis Customisation - this code will place tick marks ok the lower x axis at 1,2,3,4 and labels them with a string
axis(1, at 1:4, labels=c("A","B","C","D"))

Axis Rotation - label rotation can be controlled
las=0 parallel to the axis
las=1 horizontally oriented
las=2 right angles to the axis
las=3 vertically orientated

**Plot Annotation **
title(main=str, sub=str, x;ab=str, ylab=str)
main: Main title to appear above graph
sub:  subtitle to appear below the graph
xlab: label for the x axis 
ylab: label for the y axis

framing a plot
box()  -> places a box around the plot region
box("figure")  -> draws a box around the figure region containing the plot and its margins

Change the line texture on a plot
lty  -> blank, solid, dashed, dotted, dotdash, longdash, twodash
col  -> specify the line width and colour

Drawing Points - specify the plotting symbol the argument xaxs specifies the style of axis and interval calculation
lty
points(x, y, pch=int, col=str)

Drawing a legend
legend(xloc, yloc, legend=text, lty=linetypes, lwd=linewidths, pch=glyphname, col=colours, justification, yjust=justification)
xloc and yloc provide the location cordinates for the legend

R Plot comes with the main R Language however you add additional plot packages one of the more popular ones is ggplot
